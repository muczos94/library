﻿using System;
using LibraryGoYellow.Services.Models;

namespace LibraryGoYellow.DataLayer
{
    public class FormatReaderFactory : IFormatReaderFactory
    {
        public IBooksDataProvider CreateProvider(FormatFile type)
        {
            switch (type)
            {
                case FormatFile.json:
                    return new JsonBooksDataProvider();
                case FormatFile.xml:
                    return new XmlBooksDataProvider();;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}
