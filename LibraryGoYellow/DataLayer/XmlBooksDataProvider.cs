﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace LibraryGoYellow.DataLayer
{
    public class XmlBooksDataProvider : IBooksDataProvider
    {
        public List<Book> GetListOfBooksFromFile(string path)
        {
	        var serializer = new XmlSerializer(typeof(List<Book>));
            var tr = new StreamReader(path);
            var listOfBooks = (List<Book>) serializer.Deserialize(tr);
            tr.Close();

            return listOfBooks;
        }

        public void UpdateCatalogOfBooks(string path, List<Book> books)
        {
            using (var stream = new FileStream(path, FileMode.Create))
            {
                var xml = new XmlSerializer(typeof(List<Book>));
                xml.Serialize(stream, books);
            }
        }
    }
}
