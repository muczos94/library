﻿using LibraryGoYellow.Services.Models;

namespace LibraryGoYellow.DataLayer
{
    public  interface IFormatReaderFactory
    {
        IBooksDataProvider CreateProvider(FormatFile type);
    }
}