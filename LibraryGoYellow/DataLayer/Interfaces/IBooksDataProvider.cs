using System.Collections.Generic;

namespace LibraryGoYellow.DataLayer
{
    public  interface IBooksDataProvider
    {
        List<Book> GetListOfBooksFromFile(string path);
        void UpdateCatalogOfBooks(string path, List<Book> books);
    }
}