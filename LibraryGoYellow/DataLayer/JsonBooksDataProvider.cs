﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace LibraryGoYellow.DataLayer
{
    public class JsonBooksDataProvider : IBooksDataProvider
    {
        public List<Book> GetListOfBooksFromFile(string path)
        {
	        using (var file = new StreamReader(path))
            {
                var json = file.ReadToEnd();
                var listOfBooks = JsonConvert.DeserializeObject<List<Book>>(json);
                
                return listOfBooks;
            }
        }

        public void UpdateCatalogOfBooks(string path, List<Book> books)
        {
            using (var stream = new StreamWriter(path))
            {
                stream.Write(JsonConvert.SerializeObject(books, Formatting.Indented));
            }
        }
    }
}
