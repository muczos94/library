﻿using LibraryGoYellow.DataLayer;
using Ninject.Modules;

namespace LibraryGoYellow.Services
{
    class Modules : NinjectModule
    {
        public override void Load()
        {
            Bind<IBookService>().To<BookService>();
            Bind<IFormatReaderFactory>().To<FormatReaderFactory>();
        }
    }
}
