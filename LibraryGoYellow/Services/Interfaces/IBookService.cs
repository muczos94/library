﻿using System.Collections.Generic;

namespace LibraryGoYellow
{
    public interface IBookService
    {
        void AddBook(Book book);
        bool DeleteBook(long isbnNumber);
        Book FindBookByTitle(string title);
        Book FindBookByAuthor(Person author);
        Book FindBookByIsbn(long isbn);
        bool RentABook(long isbn, Person person);
        List<Book> FindBookByTypedWeeks(long weeks);
        Dictionary<Person, int> DisplayListOfRenter();
        bool CheckIfIsbnNumberIsUnique(long number);
    }
}