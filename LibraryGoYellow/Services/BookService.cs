﻿using System;
using System.Collections.Generic;
using System.Linq;
using LibraryGoYellow.DataLayer;

namespace LibraryGoYellow
{
	public class BookService : IBookService
	{
		private readonly IFormatReaderFactory _iFormatReaderFactory;

		public BookService()
		{

		}

		public BookService(IFormatReaderFactory iFormatReaderFactory)
		{
			_iFormatReaderFactory = iFormatReaderFactory;

		}

		public bool CheckIfIsbnNumberIsUnique(long number)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			if (!listOfBooks.Exists(x => x.NumberIsbn == number))
				return false;
			Console.WriteLine($"Number {number}, already exists. You need to type unique isbn number");
			return true;
		}

		public void AddBook(Book book)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			listOfBooks.Add(book);
			_iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).UpdateCatalogOfBooks(ProgramLoop.Path, listOfBooks);
		}

		public bool DeleteBook(long isbnNumber)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var book = listOfBooks.FirstOrDefault(x => x.NumberIsbn == isbnNumber);

			if (book == null)
				return false;

			listOfBooks.Remove(book);
			_iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).UpdateCatalogOfBooks(ProgramLoop.Path, listOfBooks);
			return true;
		}

		public Book FindBookByTitle(string title)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var book = listOfBooks.FirstOrDefault(x => x.Title == title);

			Console.WriteLine($"Book '{title}', doesn't exists in catalog");
			return book;
		}

		public Book FindBookByAuthor(Person author)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var book = listOfBooks.FirstOrDefault(x => x.Author.Name == author.Name && x.Author.Surname == author.Surname);

			if (book == null)
				Console.WriteLine($"Author: '{ author.Name} { author.Surname}', doesn't exists in catalog");

			return book;
		}

		public Book FindBookByIsbn(long isbn)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var book = listOfBooks.FirstOrDefault(x => x.NumberIsbn == isbn);

			if (book == null)
				Console.WriteLine($"Book with ISBN number: '{isbn}', doesn't exists in catalog");

			return book;
		}

		public bool RentABook(long isbn, Person person)
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var bookToRent = listOfBooks.FirstOrDefault(x => x.NumberIsbn == isbn);

			if (bookToRent == null) 
				return false;
			foreach (var book in listOfBooks
				.Where(book => book.NumberIsbn == isbn))
			{
				book.DateOfLastRent = DateTime.Now;
				book.Borrower.Name = person.Name;
				book.Borrower.Surname = person.Surname;
			}
			_iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).UpdateCatalogOfBooks(ProgramLoop.Path, listOfBooks);
			return true;
		}

		public List<Book> FindBookByTypedWeeks(long weeks)
		{
			const int daysInWeek = 7;
			var daysToSubtracts = weeks * daysInWeek;
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var dateToCheck = DateTime.Now.AddDays(-daysToSubtracts);
			var listOfBooksToRent = listOfBooks.Where(x => x.DateOfLastRent < dateToCheck).ToList();

			return listOfBooksToRent;
		}

		public Dictionary<Person, int> DisplayListOfRenter()
		{
			var listOfBooks = _iFormatReaderFactory.CreateProvider(ProgramLoop.FormatFile).GetListOfBooksFromFile(ProgramLoop.Path);
			var listOfBorrowers = new Dictionary<Person, int>();

			foreach (var book in listOfBooks)
			{
				if (book.Borrower != null)
				{
					try
					{
						var borrower = listOfBorrowers.SingleOrDefault(x => x.Key.Name.Equals(book.Borrower.Name) && x.Key.Surname.Equals(book.Borrower.Surname));
						if (borrower.Value == 0)
						{
							const int borrowedBooks = 1;
							listOfBorrowers.Add(book.Borrower, borrowedBooks);
						}
						else
						{
							var key = listOfBorrowers.SingleOrDefault(x => x.Key.Name.Equals(book.Borrower.Name) &&
																		   x.Key.Surname.Equals(book.Borrower.Surname)).Key;
							listOfBorrowers[key]++;
						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e);
					}

				}
			}

			return listOfBorrowers;
		}
	}
}
