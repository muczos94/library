﻿namespace LibraryGoYellow.Services.Models
{
    public enum FormatFile
    {
        json,
        xml
    }
}
