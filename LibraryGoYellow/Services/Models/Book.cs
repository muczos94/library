﻿using System;

namespace LibraryGoYellow
{
    public class Book
    {
        public string Title { get; set; }
        public Person Author { get; set; }
        public long NumberIsbn { get; set; }
        public DateTime DateOfLastRent { get; set; }
        public Person Borrower { get; set; }
    }
}
