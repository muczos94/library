﻿using System;
using LibraryGoYellow.Cli;
using LibraryGoYellow.Services.Models;

namespace LibraryGoYellow
{
    public class ProgramLoop
    {
        public static string Path { get; private set; } 
        public static string Extension { get; private set; }
        public static FormatFile FormatFile { get; private set; }

        private readonly IBookService _IbookService;

        public enum Commands
        {
            AddBook = 1,
            DeleteBook = 2,
            FindBookByTitle = 3,
            FindBookByAuthor = 4,
            FindBookByIsbn = 5,
            RentBook = 6,
            FindNotBorrowedBooksByWeek = 7,
            DisplayListOfRenter = 8,
            Q
        }

        public ProgramLoop(IBookService ibookService)
        {
            _IbookService = ibookService;
        }

        public void Run(string[] args)
        {
            CheckIfArgsExists(args);

            var exit = false;

            while (!exit)
            {
                var command = ConsoleReadHelper.GetCommandType();

                switch (command)
                {
                    case Commands.AddBook:
                        AddBookFunc();
                        break;
                    case Commands.DeleteBook:
                        DeleteBookFunc();
                        break;
                    case Commands.FindBookByTitle:
                        FindBookByTitleFunc();
                        break;
                    case Commands.FindBookByAuthor:
                        FindBookByAuthorFunc();
                        break;
                    case Commands.FindBookByIsbn:
                        FindBookByIsbnFunc();
                        break;
                    case Commands.FindNotBorrowedBooksByWeek:
                        FindNotBorrowedBooksByWeekFunc();
                        break;
                    case Commands.RentBook:
                        RentABookFunc();
                        break;
                    case Commands.DisplayListOfRenter:
                        DisplayListOfRenter();
                        break;
                    case Commands.Q:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine($"There is no '{command}',  try again: ");
                        break;
                }
            }
        }

        public void CheckIfArgsExists(string[] args)
        {
            if (args.Length == 0)
            {
                Path = ConsoleReadHelper.GetPath("Write path with catalog of books: ");
            }
            else
            {
                Path = args[0];
            }
            SetFileExtension(args);
        }

        public void SetFileExtension(string[] args)
        {
            if (args.Length!=0)
            {
                Extension = System.IO.Path.GetExtension(args[0]).TrimStart('.');
            }
            else
            {
                Extension = System.IO.Path.GetExtension(Path).TrimStart('.');
            }
           

            if (Extension.Equals("json"))
            {
                FormatFile = FormatFile.json;
            }
            else
            {
                FormatFile = FormatFile.xml;
            }
        }

        private void FindNotBorrowedBooksByWeekFunc()
        {
            var weeks = ConsoleReadHelper.GetLong("Enter the number of weeks: ");
            var listOfBooks =_IbookService.FindBookByTypedWeeks(weeks);
            foreach (var book in listOfBooks)
            {
                ConsoleWriteHelper.DisplayBookInfo(book);
            }
        }

        private void DisplayListOfRenter()
        {
            Console.Clear();
            Console.WriteLine("\t----- List Of Borrower ----- \n");
            var collectionOfRenter = _IbookService.DisplayListOfRenter();
            ConsoleWriteHelper.DisplayDictionaryOfBorrowers(collectionOfRenter);
        }

        private void RentABookFunc()
        {
            Console.Clear();
            Console.WriteLine("\t----- Borrow Book ----- \n");
            var person = new Person();
            person.Name = ConsoleReadHelper.GetString("Type your name: ");
            person.Surname = ConsoleReadHelper.GetString("Type your surnname: ");
            var isbnNumber = ConsoleReadHelper.GetLong("Which book do you want to rent?\n" +
                                                       "Type ISBN number [13 digits]: ");
            
            if (!_IbookService.RentABook(isbnNumber, person))
            {
                Console.WriteLine($"There is no book with ISBN: {isbnNumber}");
            }
        }

        private void FindBookByTitleFunc()
        {
            var title = ConsoleReadHelper.GetString("Type title: ");
            var book = _IbookService.FindBookByTitle(title);
            if (book != null)
            {
                ConsoleWriteHelper.DisplayBookInfo(book);
            }
        }

        private void FindBookByAuthorFunc()
        {
            var author = new Person();
            author.Name = ConsoleReadHelper.GetString("Type Author's name: ");
            author.Surname = ConsoleReadHelper.GetString("Type Author's surname: ");

            var book = _IbookService.FindBookByAuthor(author);
            if (book != null)
            {
                ConsoleWriteHelper.DisplayBookInfo(book);
            }
        }

        private void FindBookByIsbnFunc()
        {
            var isbnNumber = ConsoleReadHelper.GetIsbnNumber("Type ISBN number [13 digits]: ");
            var book = _IbookService.FindBookByIsbn(isbnNumber);
            if (book != null)
            {
                ConsoleWriteHelper.DisplayBookInfo(book);
            }
        }

        private void DeleteBookFunc()
        {
            var isbnNumber = ConsoleReadHelper.GetIsbnNumber("Type ISBN number to remove book: ");
            var bookRemoved = _IbookService.DeleteBook(isbnNumber);

            ConsoleWriteHelper.OperationSuccessful(bookRemoved);

            if (!bookRemoved)
            {
                Console.WriteLine($"There is no book with {isbnNumber} number");
            }
        }

        public void AddBookFunc()
        {
            var book = new Book();
            var author = new Person();
            var borrower = new Person();

            book.Title = ConsoleReadHelper.GetString("Type the title of the book: ");
            author.Name = ConsoleReadHelper.GetString("Type Author's name: ");
            author.Surname = ConsoleReadHelper.GetString("Type Author's surname: ");
            book.Author = author;
            do
            {
                book.NumberIsbn = ConsoleReadHelper.GetIsbnNumber("Type ISBN number [13 digits]: ");
            } while (_IbookService.CheckIfIsbnNumberIsUnique(book.NumberIsbn));
            borrower.Name = "-";
            borrower.Surname = "-";
            book.Borrower = borrower;

            _IbookService.AddBook(book);
        }
    }
}