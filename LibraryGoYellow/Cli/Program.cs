﻿using LibraryGoYellow.Services;
using Ninject;

namespace LibraryGoYellow
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel(new Modules());
            kernel.Get<ProgramLoop>().Run(args);
        }
    }

}
