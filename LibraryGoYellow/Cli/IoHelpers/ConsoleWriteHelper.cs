﻿using System;
using System.Collections.Generic;

namespace LibraryGoYellow.Cli
{
    internal class ConsoleWriteHelper
    {
        public static void DisplayMainMenu()
        {
            Console.WriteLine("------------------------Menu------------------------\n\n" +
                              "1: AddBook - adding book to catalog,\n" +
                              "2: DeleteBook - delete book from catalog\n" +
                              "3: FindBookByTitle - finding book by title\n" +
                              "4: FindBookByAuthor - finding book by Author\n" +
                              "5: FindBookByIsbn - finding book by Isbn number\n" +
                              "6: RentBook - renting a book\n" +
                              "7: FindNotBorrowedBooksByWeek - function allows find books which are not borrowed\n" +
                              "8: DisplayListOfRenter - displaying list of renters\n" +
                              "Q: Q - Closing application\n");
        }

        public static void DisplayBookInfo(Book book)
        {
            Console.WriteLine($"Title: {book.Title}\n" +
                              $"Author: {book.Author.Name} {book.Author.Surname}\n" +
                              $"ISBN: {book.NumberIsbn}\n" +
                              $"Last Rent: {book.DateOfLastRent}\n" +
                              $"Borrower: {book.Borrower.Name} {book.Borrower.Surname}\n\n");
        }

        public static void DisplayDictionaryOfBorrowers(Dictionary<Person, int> listOfBorrowers)
        {
            foreach (var borrower in listOfBorrowers)
            {
                if (!borrower.Key.Name.Equals("-"))
                {
                    Console.WriteLine($"{borrower.Key.Name} {borrower.Key.Surname}: {borrower.Value} books");
                }
            }
        }

        public static void OperationSuccessful(bool operation)
        {
            Console.WriteLine(operation ? "The operation was successful." : "Operation failed.");
        }
    }
}
