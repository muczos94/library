﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using LibraryGoYellow.DataLayer;

namespace LibraryGoYellow.Cli
{
    internal class ConsoleReadHelper
    {
        public static ProgramLoop.Commands GetCommandType()
        {
            ProgramLoop.Commands commandType;

            ConsoleWriteHelper.DisplayMainMenu();

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("You typed, wrong command. Try again..");
            }

            return commandType;
        }

        public static string GetString(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                var title = Console.ReadLine();

                while (String.IsNullOrEmpty(title))
                {
                    Console.WriteLine("You didn't write proper title... ");
                    Console.Write(message);
                    title = Console.ReadLine();
                }

                return title;
            }
        }

        public static long GetLong(string message)
        {
            long number;
            Console.Write(message);

            while (!long.TryParse(Console.ReadLine(), out number))
            {
                Console.Write("You wrote, incorrect number, try again: ");
                Console.Write(message);
            }

            return number;
        }

        public static long GetIsbnNumber(string message)
        {
            Console.Write(message);
            var regex = new Regex(@"^\d{13}$");
            long number = GetLong(message);
            while (!regex.IsMatch(number.ToString()))
            {
                Console.Write("You wrote less digits, Isbn number has 13 digits: ");
                number = GetLong(message);
            }

            return number;
        }

        public static DateTime GetDate(string message)
        {
            DateTime dateTime;
            Console.Write(message);

            while (!DateTime.TryParse(Console.ReadLine(), out dateTime))
            {
                Console.Write("You typed, incorrect date..\n\n");
                Console.Write(message);
            }

            return dateTime;
        }

        public static string GetPath(string message)
        {
            var path = GetString(message);

            while (!File.Exists(path))
            {
                Console.WriteLine("Your file, doesn't exists!");
                path = GetString("Write correct path: ");
            }
            Console.Clear();

            return path;
        }
    }
}
